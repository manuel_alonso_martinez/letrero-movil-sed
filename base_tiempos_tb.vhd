--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   19:35:08 01/25/2016
-- Design Name:   
-- Module Name:   C:/Users/lcastedo/Documents/pruebas/sed/letrero/base_tiempos_tb.vhd
-- Project Name:  letrero
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: clk_divider
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity base_tiempos_tb is
end base_tiempos_tb;
 
architecture behavior of base_tiempos_tb is 

    -- Component Declaration for the Unit Under Test (UUT)
    component base_tiempos is
        generic (
            freq_in  : positive;
            freq_out1: positive;
            freq_out2: positive;
            freq_out3: positive
        );
        port (
            reset  : in  std_logic;
            clk    : in  std_logic;
            ce     : in  std_logic;
            ce_out1: out std_logic;
            ce_out2: out std_logic;
            ce_out3: out std_logic
        );
    end component;

    --Inputs
    signal reset  : std_logic;
    signal clk_in : std_logic;
    signal ce     : std_logic;

    --Outputs
    signal ce_out1: std_logic;
    signal ce_out2: std_logic;
    signal ce_out3: std_logic;

    -- Clock period definitions
    constant TB_FREQ_IN    : positive := 50000000;
    constant TB_CE1_FREQ   : positive :=  5000000;
    constant TB_CE2_FREQ   : positive :=  1000000;
    constant TB_CE3_FREQ   : positive :=   250000;
    constant CLK_IN_PERIOD : time := 1 sec / TB_FREQ_IN;
    constant CLK_CE1_PERIOD: time := 1 sec / TB_CE1_FREQ;
    constant CLK_CE2_PERIOD: time := 1 sec / TB_CE2_FREQ;
    constant CLK_CE3_PERIOD: time := 1 sec / TB_CE3_FREQ;

    procedure measure_period(signal subject: std_logic; T: out time) is
        variable t0, t1: time;
    begin
        wait until subject = '1';
        t0 := now;
        wait until subject = '1';
        t1 := now;
        T := t1 - t0;
    end;
    
begin

    -- Instantiate the Unit Under Test (UUT)
    uut: base_tiempos
        generic map (
            freq_in   => TB_FREQ_IN,
            freq_out1 => TB_CE1_FREQ,
            freq_out2 => TB_CE2_FREQ,
            freq_out3 => TB_CE3_FREQ
        )
        port map (
            reset   => reset,
            clk     => clk_in,
            ce      => '1',
            ce_out1 => ce_out1,
            ce_out2 => ce_out2,
            ce_out3 => ce_out3
        );

    -- Clock process definitions
    clk_in_process :process
    begin
        clk_in <= '0';
        wait for 0.5 * CLK_IN_PERIOD;
        clk_in <= '1';
        wait for 0.5 * CLK_IN_PERIOD;
    end process;

    -- Stimulus process
    reset <= '1' after 0.25 * CLK_IN_PERIOD, '0' after 0.75 * CLK_IN_PERIOD;
    ce    <= '1' after 0.25 * CLK_IN_PERIOD;

    stim_proc: process
        variable t0, t1: time;
    begin
      wait until reset = '0';
      wait for 2.25 * CLK_CE3_PERIOD;
      assert false
        report "[SUCCESS]: Simulation finished."
        severity failure;
    end process;

    -- Check periods
    check1_proc: process
        variable T: time;
    begin
      wait until reset = '0';
      measure_period(ce_out1, T);
      assert T = CLK_CE1_PERIOD
          report "[FAILED]: Wrong ce_out1 frequency."
          severity error;
    end process;

    check2_proc: process
        variable T: time;
    begin
      wait until reset = '0';
      measure_period(ce_out2, T);
      assert T = CLK_CE2_PERIOD
          report "[FAILED]: Wrong ce_out2 frequency."
          severity error;
    end process;

    check3_proc: process
        variable T: time;
    begin
      wait until reset = '0';
      measure_period(ce_out3, T);
      assert T = CLK_CE3_PERIOD
          report "[FAILED]: Wrong ce_out3 frequency."
          severity error;
    end process;

end;
