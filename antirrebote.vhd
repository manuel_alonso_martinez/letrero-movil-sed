----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:37:07 01/26/2016 
-- Design Name: 
-- Module Name:    antirrebote - behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity antirrebote is
    port (
        reset: in  std_logic;
        clk  : in  std_logic;
        ce   : in  std_logic;
        din  : in  std_logic;
        dout : out std_logic
    );
end antirrebote;

architecture behavioral of antirrebote is
    signal reg: std_logic_vector(2 downto 0);

    attribute ASYNC_REG: string;
    attribute ASYNC_REG of reg: signal is "TRUE";
begin
    process(reset, clk)
    begin
        if reset = '1' then
            reg  <= (others => '0');
            dout <= '0';
        elsif rising_edge(clk) then
            dout <= '0';
            if ce = '1' then
                reg <= reg(1 downto 0) & din;
                if reg = "011" then
                    dout <= '1';
                end if;
            end if;
        end if;
    end process;
end behavioral;

