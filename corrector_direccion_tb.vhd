--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   00:16:53 01/26/2016
-- Design Name:   
-- Module Name:   C:/Users/lcastedo/Documents/pruebas/sed/letrero/corrector_direccion_tb.vhd
-- Project Name:  letrero
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: corrector_direccion
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.ALL;
 
entity corrector_direccion_tb is
end corrector_direccion_tb;
 
architecture behavior of corrector_direccion_tb is 
 
    -- Component Declaration for the Unit Under Test (UUT)
    component corrector_direccion is
        generic (
            size: positive
        );
        port (
            base   : in  unsigned(4 downto 0);
            digit  : in  unsigned(1 downto 0);
            address: out unsigned(4 downto 0)
        );
    end component;

    component contador_binario is
        generic (
            width: positive
        );
        port (
            reset: in  std_logic;
            clk  : in  std_logic;
            ce   : in  std_logic;
            q    : out unsigned(width - 1 downto 0)
        );
    end component;

   --Inputs
   signal base : unsigned(4 downto 0) := (others => '0');
   signal digit: unsigned(1 downto 0) := (others => '0');

 	--Outputs
   signal address : unsigned(4 downto 0);

   signal reset: std_logic; 
   signal clk  : std_logic; 
 
   constant clk_period: time := 1 sec / 50000000;
   constant delay     : time := 0.1 * clk_period;

   constant tb_base   : positive := 17;

begin
    digit_cntr: contador_binario
        generic map (
            width => 2
        )
        port map (
            reset => reset,
            clk   => clk,
            ce    => '1',
            q     => digit
        );

    -- Instantiate the Unit Under Test (UUT)
    corrector: corrector_direccion
        generic map (
            size => tb_base
        )
        port map (
            base    => base,
            digit   => digit,
            address => address
        );

    -- Clock process definitions
    clk_process: process
    begin
        clk <= '0';
        wait for 0.5 * clk_period;
        clk <= '1';
        wait for 0.5 * clk_period;
    end process;

    reset <= '1' after 0.25 * clk_period, '0' after 0.75 * clk_period;
    
    -- Stimulus process
    stim_proc: process
    begin
        base <= to_unsigned(tb_base - 2, base'length);
        wait until reset = '0';
        for i in 1 to 5 loop
          wait on digit;
          wait for delay;
          assert to_integer(address) = (to_integer(base) + to_integer(digit)) mod tb_base
              report "[FAILURE]: address not corrected."
              severity failure;
        end loop;
        wait for 2 * clk_period;
        assert false
            report "[SUCCESS]: simulation finished."
            severity failure;
    end process;

end;
