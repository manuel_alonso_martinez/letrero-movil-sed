----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:06:34 01/13/2016 
-- Design Name: 
-- Module Name:    clk_divider - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity clk_divider is
  generic (
    freq_in : positive;
    freq_out: positive
  );
  port (
    reset  : in  std_logic;
    clk_in : in  std_logic;
    ce     : in  std_logic;
    clk_out: out std_logic
  );
end clk_divider;

architecture behavioral of clk_divider is
  constant factor: positive := freq_in / freq_out;
  subtype count_t is integer range 0 to factor - 1;
begin
  process(reset, clk_in)
    variable count: count_t;
  begin
    if reset = '1' then
      count := 0;
      clk_out <= '0';
    elsif rising_edge(clk_in) then
        clk_out <= '0';
      if ce = '1' then
        if count /= 0 then
          count := count - 1;
        else
          count := count_t'high;
        end if;
        if count = 0 then
          clk_out <= '1';
        end if;
      end if;
    end if;
  end process;
end behavioral;
