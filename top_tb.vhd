--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:01:14 01/25/2016
-- Design Name:   
-- Module Name:   C:/Users/sed/Desktop/rotuloV58/top_tb.vhd
-- Project Name:  rotulo_movil
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
--use IEEE.numeric_std.all;

entity top_tb is
end top_tb;

architecture behavior of top_tb is 
 
    -- Component Declaration for the Unit Under Test (UUT)
    component top is
        generic (
            freq_in : positive
        );
        port (
            clk      : in  std_logic;
            reset    : in  std_logic;
            selected : in  std_logic;
            up       : in  std_logic;
            down     : in  std_logic;
            mode     : in  std_logic;
            segmentos: out std_logic_vector(7 downto 0);
            digitos  : out std_logic_vector(3 downto 0)
        );
    end component;

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal selected : std_logic := '0';
   signal up : std_logic := '0';
   signal down : std_logic := '0';
   signal mode : std_logic := '0';

 	--Outputs
   signal segmentos : std_logic_vector(6 downto 0);
   signal digitos : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_freq  : positive := 2000;  -- Hz
   constant clk_period: time := 1 sec / clk_freq;

begin

    -- Instantiate the Unit Under Test (UUT)
    uut: top
        generic map (
            freq_in   => clk_freq
        )
        port map (
            clk       => clk,
            reset     => reset,
            selected  => selected,
            up        => up,
            down      => down,
            mode      => mode,
            segmentos => segmentos,
            digitos   => digitos
        );

    -- Clock process definitions
    clk_process :process
    begin
        clk <= '0';
        wait for 0.5 * clk_period;
        clk <= '1';
        wait for 0.5 * clk_period;
    end process;
 
   -- Stimulus process
    reset <= '1' after 0.25 * clk_period, '0' after 0.75 * clk_period;

    stim_proc: process
    begin		
        wait until reset = '0';  -- Esperar final del reset

        -- TODO: tiempos demasiado cortos, no realistas
        up <= '1';
        wait for 11 ns;
        up <= '0';
        wait for 11 ns;
        up <= '1';
        wait for 11 ns;
        up <= '0';
        wait for 11 ns;
        up <= '1';
        wait for 11 ns;
        up <= '0';
        wait for 11 ns;
        selected <= '1';
        wait for 25 ns;
        selected <= '0';
        wait for 5 ns;
        up <= '1';
        wait for 11 ns;
        up <= '0';
        wait for 11 ns;
        up <= '1';
        wait for 11 ns;
        up <= '0';
        wait for 11 ns;
        up <= '1';
        wait for 11 ns;
        up <='0';
        wait for 11 ns;
        selected <= '1';
        wait for 20 ns;
        selected <= '0';
        wait for 5 ns;
        up <= '1';
        wait for 11 ns;
        up <= '0';
        wait for 11 ns;
        up <= '1';
        wait for 11 ns;
        up <= '0';
        wait for 11 ns;
        up <= '1';
        wait for 11 ns;
        up <= '0';
        wait for 11 ns;
        selected <= '1';
        wait for 15 ns;
        selected <= '0';
        wait for 5 ns;
        up <= '1';
        wait for 11 ns;
        up <= '0';
        wait for 11 ns;
        up <= '1';
        wait for 11 ns;
        up <= '0';
        wait for 11 ns;
        up <= '1';
        wait for 11 ns;
        up <= '0';
        wait for 11 ns;
        selected <= '1';
        wait for 35 ns;
        selected <= '0';
        wait for 5 ns;
        up <= '1';
        wait for 11 ns;
        up <= '0';
        wait for 11 ns;
        up <= '1';
        wait for 11 ns;
        up <= '0';
        wait for 11 ns;
        up <= '1';
        wait for 11 ns;
        up <= '0';
        wait for 11 ns;
        selected <= '1';
        wait for 25 ns;
        selected <= '0';
        mode <= '1';

        -- TODO: Completar el test

        wait for 100 * CLK_PERIOD;
        assert false
            report "[SUCCESS]: Simulation finished."
            severity failure;
    end process;

end;
