--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   19:35:08 01/25/2016
-- Design Name:   
-- Module Name:   C:/Users/lcastedo/Documents/pruebas/sed/letrero/clk_divider_tb.vhd
-- Project Name:  letrero
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: clk_divider
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity clk_divider_tb is
end clk_divider_tb;
 
architecture behavior of clk_divider_tb is 

    -- Component Declaration for the Unit Under Test (UUT)
    component clk_divider is
        generic (
            freq_in : positive;
            freq_out: positive
        );
        port (
            reset  : in  std_logic;
            clk_in : in  std_logic;
            ce     : in  std_logic;
            clk_out: out std_logic
        );
    end component;

    --Inputs
    signal reset  : std_logic;
    signal clk_in : std_logic;
    signal ce     : std_logic;

    --Outputs
    signal clk_out: std_logic;

    -- Clock period definitions
    constant tb_freq_in    : positive := 50000000;
    constant tb_freq_out   : positive :=  5000000;
    constant clk_in_period : time := 1 sec / tb_freq_in;
    constant clk_out_period: time := 1 sec / tb_freq_out;

    constant delay         : time := 0.1 * clk_in_period;
  
    constant factor        : positive := tb_freq_in / tb_freq_out;

begin
 
    -- Instantiate the Unit Under Test (UUT)
    uut: clk_divider
        generic map (
            freq_in  => tb_freq_in,
            freq_out => tb_freq_out
        )
        port map (
            reset   => reset,
            clk_in  => clk_in,
            ce      => ce,
            clk_out => clk_out
        );

    -- Clock process definitions
    clk_in_process: process
    begin
        clk_in <= '0';
        wait for 0.5 * clk_in_period;
        clk_in <= '1';
        wait for 0.5 * clk_in_period;
    end process;

    -- Stimulus process
    reset <= '1' after 0.25 * clk_in_period, '0' after 0.75 * clk_in_period;
    ce    <= '0' after 0.25 * clk_in_period, '1' after 2.25 * clk_in_period;
  
    stim_proc: process
        variable t0, t1: time;
    begin
        wait until reset = '0';
        for i in 1 to factor - 1 loop
            wait until clk_in = '1';
        end loop;
        wait for delay;
        assert clk_out = '0'
            report "[FAILED]: CE non functional."
            severity error;
        wait until clk_out = '1';
        t0 := now;
        wait until clk_out = '1';
        t1 := now;
        assert t1 - t0 = clk_out_period
            report "[FAILED]: Wrong output frequency."
            severity failure;
        wait for 2 * clk_in_period;
        assert false
            report "[SUCCESS]: Simulation finished."
            severity failure;
    end process;
end;
