--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:55:35 01/26/2016
-- Design Name:   
-- Module Name:   C:/Users/lcastedo/Documents/pruebas/sed/letrero/antirrebote_tb.vhd
-- Project Name:  letrero
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: antirrebote
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity antirrebote_tb is
end antirrebote_tb;

architecture behavior of antirrebote_tb is 

    -- Component Declaration for the Unit Under Test (UUT)
    component antirrebote
        port(
            reset: in  std_logic;
            clk  : in  std_logic;
            ce   : in  std_logic;
            din  : in  std_logic;
            dout : out std_logic
        );
    end component;

    --Inputs
    signal reset: std_logic;
    signal clk  : std_logic;
    signal ce   : std_logic;
    signal din  : std_logic;

    --Outputs
    signal dout : std_logic;

    -- Clock period definitions
    constant TB_FREQ_IN: positive := 50000000;
    constant CLK_PERIOD: time := 1 sec / TB_FREQ_IN;
    constant DELAY     : time := 0.1 * CLK_PERIOD;

begin

    -- Instantiate the Unit Under Test (UUT)
    uut: antirrebote
        port map (
            reset => reset,
            clk   => clk,
            ce    => ce,
            din   => din,
            dout  => dout
        );

    -- Clock process definitions
    clk_process :process
    begin
        clk <= '0';
        wait for 0.5 * CLK_PERIOD;
        clk <= '1';
        wait for 0.5 * CLK_PERIOD;
    end process;

    ce_process: process
    begin
        ce <= '0' after 0.5 * CLK_PERIOD + DELAY;
        wait for 9 * CLK_PERIOD;
        ce <= '1' after 0.5 * CLK_PERIOD + DELAY;
        wait for 1 * CLK_PERIOD;
    end process;

    reset <= '1' after 0.25 * CLK_PERIOD, '0' after 0.75 * CLK_PERIOD;

    -- Stimulus process
    stim_proc: process
    begin
        din <= '0';
        wait until reset = '0';
        for i in 1 to 5 loop
            wait until clk = '1';
        end loop;
        wait for 0.75 * CLK_PERIOD;
        din <= '1';
        wait for 50 * CLK_PERIOD;
        din <= '0';
    end process;

    -- Check process
    check_proc: process
        variable tref: time;
    begin
        wait until reset = '0';
        wait until din = '1';
        tref := now;
        for i in 1 to 3 loop
            wait until ce = '1';
        end loop;
        wait until clk = '1';
        assert dout'last_event >= 30 * CLK_PERIOD
            report "[FAILED]: dout activated too soon."
            severity failure;
        wait for DELAY;
        assert dout = '1'
            report "[FAILED]: dout activated too late."
            severity failure;
        wait until clk = '1';
        wait for DELAY;
        assert dout = '0'
            report "[FAILED]: dout deactivated too late."
            severity failure;
        wait for 50 * CLK_PERIOD;

        assert false
            report "[SUCCESS]: Simulation finished."
            severity failure;
    end process;

end;
