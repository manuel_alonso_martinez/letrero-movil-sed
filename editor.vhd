----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    02:02:35 01/26/2016 
-- Design Name: 
-- Module Name:    editor - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity editor is
    port (
        reset: in  std_logic;
        clk  : in  std_logic;
        ce   : in  std_logic;
        up   : in  std_logic;
        down : in  std_logic;
        q    : out std_logic_vector (4 downto 0)
    );
end editor;

architecture Behavioral of editor is
    signal q_i: unsigned(q'range);
begin
    process (reset, clk)
    begin
      if reset = '1' then
          q_i <= (others => '0');
      elsif rising_edge(clk) then
          if ce = '1' then
              if up = '1' then
                  q_i <= q_i + 1;
              elsif down = '1' then
                  q_i <= q_i - 1;
              end if;
          end if;
      end if;
    end process;
    q <= std_logic_vector(q_i);
end behavioral;
