----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:34:36 01/26/2016 
-- Design Name: 
-- Module Name:    base_tiempos - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity base_tiempos is
    generic (
        freq_in  : positive;
        freq_out1: positive;
        freq_out2: positive;
        freq_out3: positive
    );
    port (
        reset  : in  std_logic;
        clk    : in  std_logic;
        ce     : in  std_logic;
        ce_out1: out std_logic;
        ce_out2: out std_logic;
        ce_out3: out std_logic
    );
end base_tiempos;

architecture structural of base_tiempos is

    component clk_divider is
        generic (
            freq_in : positive;
            freq_out: positive
        );
        port (
            reset  : in  std_logic;
            clk_in : in  std_logic;
            ce     : in  std_logic;
            clk_out: out std_logic
        );
    end component;

    signal  ce_out1_i: std_logic;
    signal  ce_out2_i: std_logic;

begin
    prescaler1: clk_divider
        generic map (
            freq_in  => freq_in,
            freq_out => freq_out1
        )
        port map (
            reset   => reset,
            clk_in  => clk,
            ce      => ce,
            clk_out => ce_out1_i
        );

    prescaler2: clk_divider
        generic map (
            freq_in  => freq_out1,
            freq_out => freq_out2
        )
        port map (
            reset   => reset,
            clk_in  => clk,
            ce      => ce_out1_i,
            clk_out => ce_out2_i
        );

    prescaler3: clk_divider
        generic map (
            freq_in  => freq_out2,
            freq_out => freq_out3
        )
        port map (
            reset   => reset,
            clk_in  => clk,
            ce      => ce_out2_i,
            clk_out => ce_out3
        );

     ce_out1 <= ce_out1_i;
     ce_out2 <= ce_out2_i;
end structural;

