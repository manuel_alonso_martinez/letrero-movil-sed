----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:57:54 12/06/2015 
-- Design Name: 
-- Module Name:    mux - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity decodificador_segmento is
    port(
        datos   : in  std_logic_vector(31 downto 0);
        codigo  : in  std_logic_vector(4 downto 0);
        segmento: out std_logic
    );
end decodificador_segmento;

architecture dataflow of decodificador_segmento is
begin
  segmento <= datos(to_integer(unsigned(codigo)));
end dataflow;

