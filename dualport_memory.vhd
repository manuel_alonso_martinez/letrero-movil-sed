----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:26:11 01/26/2016 
-- Design Name: 
-- Module Name:    dualport_memory - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity dualport_memory is
    generic (
        size: positive := 25
    );
    port (
        reset: in  std_logic;
        clk  : in  std_logic;
        din  : in  std_logic_vector (4 downto 0);
        dout : out std_logic_vector (4 downto 0);
        we   : in  std_logic;
        waddr: in  unsigned (4 downto 0);
        raddr: in  unsigned (4 downto 0)
    );
end dualport_memory;

architecture behavioral of dualport_memory is
    subtype record_t is std_logic_vector(4 downto 0);
    type memory_t is array(0 to size - 1) of record_t;
    signal bank: memory_t;
begin
    process (reset, clk)
    begin
        if reset = '1' then
            bank <= (others => "00000");
        elsif rising_edge(clk) then
            if we = '1' then
                bank(to_integer(waddr)) <= din;
            end if;
        end if;
    end process;

    dout <= bank(to_integer(raddr));
end behavioral;

