--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   19:43:44 01/20/2016
-- Design Name:   
-- Module Name:   F:/Lab.SED/rotulo_movil/visualizador_tb.vhd
-- Project Name:  rotulo_movil
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: visualizador
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity visualizador_tb is
end visualizador_tb;
 
architecture behavior of visualizador_tb is 
 
    -- Component Declaration for the Unit Under Test (UUT)
    component visualizador is
        generic (
            size     : positive
        );
        port (
            reset    : in  std_logic;
            clk      : in  std_logic;
            rfrsh_ce : in  std_logic;
            base     : in  unsigned(4 downto 0);
            codigo   : in  std_logic_vector(4 downto 0);
            dir      : out unsigned(4 downto 0);
            digitos  : out std_logic_vector(3 downto 0);
            segmentos: out std_logic_vector(7 downto 0)
         );
    end component;

    --Inputs
    signal reset    : std_logic;
    signal clk      : std_logic;
    signal rfrsh_ce : std_logic;
    signal base     : unsigned(4 downto 0);
    signal codigo   : std_logic_vector(4 downto 0);

    --Outputs
    signal dir      : unsigned(4 downto 0);
    signal digitos  : std_logic_vector(3 downto 0);
    signal segmentos: std_logic_vector(7 downto 0);

    -- Clock period definitions
    constant TB_FREQ_IN: positive := 50000000;
    constant CLK_PERIOD: time := 1 sec / TB_FREQ_IN;
    constant DELAY     : time := 0.1 * CLK_PERIOD;

    subtype codigo_t is std_logic_vector(4 downto 0);
    type mem_mock_t is array(natural range <>) of codigo_t;
    constant MEM_MOCK: mem_mock_t := (
        "00001",
        "00010",
        "00011",
        "00000",
        "00000"
    );

    signal valid_dir: unsigned(4 downto 0);  -- Evita problemas con rango de MEM_MOCK

begin
 
    -- Instantiate the Unit Under Test (UUT)
    uut: visualizador
        generic map (
            size => MEM_MOCK'length
        )
        port map (
            reset     => reset,
            clk       => clk,
            rfrsh_ce  => rfrsh_ce,
            base      => base,
            codigo    => codigo,
            dir       => dir,
            digitos   => digitos,
            segmentos => segmentos
        );

    -- Clock process definitions
    clk_process: process
    begin
        clk <= '0';
        wait for 0.5 * CLK_PERIOD;
        clk <= '1';
        wait for 0.5 * CLK_PERIOD;
    end process;

    rfrsh_ce_process: process
    begin
        rfrsh_ce <= '0' after 0.5 * CLK_PERIOD + DELAY;
        wait for 9 * CLK_PERIOD;
        rfrsh_ce <= '1' after 0.5 * CLK_PERIOD + DELAY;
        wait for 1 * CLK_PERIOD;
    end process;

    reset <= '1' after 0.25 * CLK_PERIOD, '0' after 0.75 * CLK_PERIOD;

    base <= to_unsigned(3, base'length);

    -- Simula acceso a la "memoria" del mensaje
    with to_integer(dir) select
        valid_dir <= dir when MEM_MOCK'low to MEM_MOCK'high,
                     (others => '0') when others;
    codigo <= MEM_MOCK(to_integer(valid_dir));
    
    -- Stimulus process
    stim_proc: process
    begin
        wait until reset = '0';

        -- TODO: Hacer el test

        wait for 100 * CLK_PERIOD;
        assert false
            report "[SUCCESS]: Simulation finished."
            severity failure;
    end process;

end;
