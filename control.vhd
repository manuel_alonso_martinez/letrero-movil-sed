----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    02:18:47 01/26/2016 
-- Design Name: 
-- Module Name:    control - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity control is
    port (
        reset    : in  std_logic;
        clk      : in  std_logic;
        selected : in  std_logic;
        mode     : in  std_logic;
        shift_ce : in  std_logic;
        editor_ce: out std_logic;
        mem_we   : out std_logic;
        mem_waddr: out unsigned(4 downto 0);
        base     : out unsigned(4 downto 0)
    );
end control;

architecture Behavioral of control is
    type state_t is (S0_INIT, S1_X);
    signal state, next_state: state_t;
begin
    sync_proc: process (reset, clk)
    begin
        if reset = '1' then
            state <= S0_INIT;
        elsif rising_edge(clk) then
            state <= next_state;
        end if;        
    end process;

    output_decoder: process (state)
    begin
        case state is
            when => S0_INIT
                editor_ce <= ;
                mem_we    <= ;
                mem_waddr <= ;
                base      <= ;
            when => others
                editor_ce <= ;
                mem_we    <= ;
                mem_waddr <= ;
                base      <= ;
    end process;

    next_state_decode: process (state, shift_ce, mode, selected)
    begin
        next_state <= state;
        case state is
            when S0_INIT =>
                if selected = '1' or mode = '0' then
                    next_state <= st2_<name>;
                end if;
            when st2_<name> =>
                if selected = '1' or mode = '0' then
                    next_state <= st3_<name>;
                end if;
            when st3_<name> =>
                next_state <= st1_<name>;
            when others =>
                next_state <= st1_<name>;
        end case;      
    end process;
end behavioral;
