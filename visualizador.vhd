----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:25:48 12/22/2015 
-- Design Name: 
-- Module Name:    visualizador - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity visualizador is
    generic (
        size        : positive                        -- Tama�o de la memoria del mensaje
    );
    port (
			  reset    : in  std_logic;                     -- Reset global
        clk      : in  std_logic;                     -- Reloj global
        rfrsh_ce : in  std_logic;                     -- Permiso reloj refresco display
        base     : in  unsigned(4 downto 0);          -- Direcci�n base ventana mensaje
        codigo   : in  std_logic_vector(4 downto 0);  -- C�digo mensaje[dir]
        dir      : out unsigned(4 downto 0);          -- Direcci�n del caracter a acceder
			  digitos  : out std_logic_vector(3 downto 0);  -- Control d�gitos
			  segmentos: out std_logic_vector(7 downto 0)   -- Control segmentos
		 );
end visualizador;

architecture behavioral of visualizador is

    component contador_binario is
        generic (
            width: positive
        );
        port (
            reset: in  std_logic;
            clk  : in  std_logic;
            ce   : in  std_logic;
            q    : out unsigned(width - 1 downto 0)
        );
    end component;

    component decodificador is
        port (
            code_in : in  unsigned(1 downto 0);
            code_out: out std_logic_vector(3 downto 0)
        );
    end component;

    component decodificador_digito is
        port (
            codigo   : in  std_logic_vector(4 downto 0);
            segmentos: out std_logic_vector(6 downto 0)
        );
    end component;

    component corrector_direccion is
        generic (
            size: positive
        );
        port (
            base   : in  unsigned(4 downto 0);
            digit  : in  unsigned(1 downto 0);
            address: out unsigned(4 downto 0)
        );
    end component;

    signal dig_dir : unsigned(1 downto 0);

begin
    decoder7seg: decodificador_digito
        port map (
            codigo    => codigo,
            segmentos => segmentos(6 downto 0)
        );

    segmentos(7) <= '1';  -- Switch-off digital point

    digit_update: contador_binario
        generic map (
            width => 2
        )
        port map (
            reset => reset,
            clk   => clk,
            ce    => rfrsh_ce,
            q     => dig_dir
        );

    selector_digito: decodificador
        port map (
            code_in  => dig_dir,
            code_out => digitos
        );

    corrector: corrector_direccion
        generic map (
            size => size
        )
        port map (
            base    => base,
            digit   => dig_dir,
            address => dir
        );

end behavioral;

