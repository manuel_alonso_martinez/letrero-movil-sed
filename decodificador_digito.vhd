----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:20:34 12/08/2015 
-- Design Name: 
-- Module Name:    decodificador_digito - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity decodificador_digito is
    port (
        codigo   : in  std_logic_vector(4 downto 0);
        segmentos: out std_logic_vector(6 downto 0)
    );
end decodificador_digito;

architecture behavioral of decodificador_digito is

    -- Todos con logica negada
    constant DATOS_A: std_logic_vector(31 downto 0) := "01110100101100100010100000010011";    
    constant DATOS_B: std_logic_vector(31 downto 0) := "00111100111101011101100001100001";    
    constant DATOS_C: std_logic_vector(31 downto 0) := "00010101000101001101000000000101";    
    constant DATOS_D: std_logic_vector(31 downto 0) := "00000111011000101000011010010011";    
    constant DATOS_E: std_logic_vector(31 downto 0) := "01001010000000000000001010111011";    
    constant DATOS_F: std_logic_vector(31 downto 0) := "00100100111011000010000010001111";    
    constant DATOS_G: std_logic_vector(31 downto 0) := "10100000000111000001000010000011";    

    -- Utilizacion del modulo decodificador_segmento
    component decodificador_segmento
        port(
            datos   : in  std_logic_vector(31 downto 0);
            codigo  : in  std_logic_vector(4 downto 0);
            segmento: out std_logic
        );
    end component;

begin

    segmento_a: decodificador_segmento port map(
        datos    => DATOS_A,
        codigo   => codigo,
        segmento => segmentos(6)
    );

    segmento_b: decodificador_segmento port map(
        datos    => DATOS_B,
        codigo   => codigo,
        segmento => segmentos(5)
    );

    segmento_c: decodificador_segmento port map(
        datos    => DATOS_C,
        codigo   => codigo,
        segmento => segmentos(4)
    );

    segmento_d: decodificador_segmento port map(
        datos    => DATOS_D,
        codigo   => codigo,
        segmento => segmentos(3)
    );

    segmento_e: decodificador_segmento port map(
        datos    => DATOS_E,
        codigo   => codigo,
        segmento => segmentos(2)
    );

    segmento_f: decodificador_segmento port map(
        datos    => DATOS_F,
        codigo   => codigo,
        segmento => segmentos(1)
    );

    segmento_g: decodificador_segmento port map(
        datos    => DATOS_G,
        codigo   => codigo,
        segmento => segmentos(0)
    );

end behavioral;
