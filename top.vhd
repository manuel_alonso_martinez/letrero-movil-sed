----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:08:48 01/14/2016 
-- Design Name: 
-- Module Name:    top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top is
    generic (
        freq_in  : positive := 50000000  -- Hz
    );
    port (
        clk      : in  std_logic;
        reset    : in  std_logic;
        selected : in  std_logic;
        up       : in  std_logic;
        down     : in  std_logic;
        mode     : in  std_logic;
        segmentos: out std_logic_vector(7 downto 0);
        digitos  : out std_logic_vector(3 downto 0)
    );
end top;

architecture structural of top is

    constant REFRESH_RATE: positive := 400;  -- Hz
    constant KEYSCAN_RATE: positive :=  10;  -- Hz
    constant SHIFT_RATE  : positive :=   1;  -- Hz

    constant MEM_DEPTH   : positive :=  25;  -- Chars

    component base_tiempos is
        generic (
            freq_in  : positive;
            freq_out1: positive;
            freq_out2: positive;
            freq_out3: positive
        );
        port (
            reset  : in  std_logic;
            clk    : in  std_logic;
            ce     : in  std_logic;
            ce_out1: out std_logic;
            ce_out2: out std_logic;
            ce_out3: out std_logic
        );
    end component;

    component antirrebote is
        port (
            reset: in  std_logic;
            clk  : in  std_logic;
            ce   : in  std_logic;
            din  : in  std_logic;
            dout : out std_logic
        );
    end component;

    component dualport_memory is
        generic (
            size: positive
        );
        port (
            reset: in  std_logic;
            clk  : in  std_logic;
            din  : in  std_logic_vector (4 downto 0);
            dout : out std_logic_vector (4 downto 0);
            we   : in  std_logic;
            waddr: in  unsigned(4 downto 0);
            raddr: in  unsigned(4 downto 0)
        );
    end component;

    component editor is
        port (
            reset: in  std_logic;
            clk  : in  std_logic;
            ce   : in  std_logic;
            up   : in  std_logic;
            down : in  std_logic;
            q    : out std_logic_vector (4 downto 0)
        );
    end component;

    component visualizador is
        generic (
            size     : positive
        );
        port (
            reset    : in  std_logic;
            clk      : in  std_logic;
            rfrsh_ce : in  std_logic;
            base     : in  unsigned(4 downto 0);
            codigo   : in  std_logic_vector(4 downto 0);
            dir      : out unsigned(4 downto 0);
            digitos  : out std_logic_vector(3 downto 0);
            segmentos: out std_logic_vector(7 downto 0)
         );
    end component;

    component control is
        port (
            reset    : in  std_logic;
            clk      : in  std_logic;
            selected : in  std_logic;
            mode     : in  std_logic;
            shift_ce : in  std_logic;
            editor_ce: in  std_logic;
            mem_we   : out std_logic;
            mem_waddr: out unsigned(4 downto 0);
            base     : out unsigned(4 downto 0)
        );
    end component;

    signal rfrsh_ce  : std_logic;  -- Base de tiempos refresco display 
    signal keyscan_ce: std_logic;  -- Base de tiempos muestreo botones
    signal shift_ce  : std_logic;  -- Base de tiempos desplazamiento mensaje

    signal selected_i: std_logic;  -- '1' durante 1 periodo de reloj cuando
    signal up_i      : std_logic;  -- hay un flanco en las entradas.
    signal down_i    : std_logic;
    signal mode_i    : std_logic;

    signal base     : unsigned(4 downto 0);
    signal raddr    : unsigned(4 downto 0);
    signal rdata    : std_logic_vector(4 downto 0);
    signal waddr    : unsigned(4 downto 0);
    signal wdata    : std_logic_vector(4 downto 0);
    signal we       : std_logic;
    signal editor_ce: std_logic;

begin         --begin architecture
			  
    base_tiempos1: base_tiempos
        generic map (
            freq_in   => freq_in,
            freq_out1 => REFRESH_RATE,
            freq_out2 => KEYSCAN_RATE,
            freq_out3 => SHIFT_RATE
        )
        port map (
            reset   => reset,
            clk     => clk,
            ce      => '1',
            ce_out1 => rfrsh_ce,
            ce_out2 => keyscan_ce,
            ce_out3 => shift_ce
        );

    ar0: antirrebote
        port map (
            reset => reset,
            clk   => clk,
            ce    => keyscan_ce,
            din   => selected,
            dout  => selected_i
        );

    ar1: antirrebote
        port map (
            reset => reset,
            clk   => clk,
            ce    => keyscan_ce,
            din   => up,
            dout  => up_i
        );

    ar2: antirrebote
        port map (
            reset => reset,
            clk   => clk,
            ce    => keyscan_ce,
            din   => down,
            dout  => down_i
        );

    ar3: antirrebote
        port map (
            reset => reset,
            clk   => clk,
            ce    => keyscan_ce,
            din   => mode,
            dout  => mode_i
        );

    editor1: editor
        port map (
            reset => reset,
            clk   => clk,
            ce    => editor_ce,
            up    => up_i,
            down  => down_i,
            q     => wdata
        );

    memoria: dualport_memory
        generic map (
            size => MEM_DEPTH
        )
        port map (
            reset => reset,
            clk   => clk,
            din   => wdata,
            dout  => rdata,
            we    => we,
            waddr => waddr,
            raddr => raddr 
        );

    visualizador1: visualizador
        generic map (
            size => MEM_DEPTH
        )
        port map (
            reset     => reset,
            clk       => clk,
            rfrsh_ce  => rfrsh_ce,
            base      => base,
            codigo    => rdata,
            dir       => raddr,
            digitos   => digitos,
            segmentos => segmentos
        );

    control_unit: control
        port map (
            reset     => reset,
            clk       => clk,
            selected  => selected_i,
            mode      => mode_i,
            shift_ce  => shift_ce,
            editor_ce => editor_ce,
            mem_we    => we,
            mem_waddr => waddr,
            base      => base
        );

end structural;
