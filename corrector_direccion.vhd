----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:52:19 01/25/2016 
-- Design Name: 
-- Module Name:    corrector_direccion - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity corrector_direccion is
    generic (
        size: positive := 25
    );
    port (
        base   : in  unsigned(4 downto 0);
        digit  : in  unsigned(1 downto 0);
        address: out unsigned(4 downto 0)
    );
end corrector_direccion;

architecture behavioral of corrector_direccion is
begin
    process (base, digit)
        variable sum: integer;
    begin
      sum := to_integer(base) + to_integer(digit);
      if sum < size then
          address <= to_unsigned(sum, address'length);
      else
          address <= to_unsigned(sum - size, address'length);
      end if;
    end process;
end behavioral;
