----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:29:40 01/25/2016 
-- Design Name: 
-- Module Name:    contador_binario - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity contador_binario is
    generic (
        width: positive := 4
    );
    port (
        reset: in  std_logic;
        clk  : in  std_logic;
        ce   : in  std_logic;
        q    : out unsigned(width - 1 downto 0)
    );
end contador_binario;

architecture behavioral of contador_binario is
    signal q_i: unsigned(q'range);
begin
    process(reset, clk)
    begin
        if reset = '1' then
            q_i <= (others => '0');
        elsif rising_edge(clk) then
            if ce = '1' then
                q_i <= q_i + 1;
            end if;
        end if;
    end process;
    q <= q_i;
end behavioral;
