----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:04:20 01/14/2016 
-- Design Name: 
-- Module Name:    decodificador - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity decodificador is
    port (
        code_in : in  unsigned(1 downto 0);
        code_out: out std_logic_vector(3 downto 0)
    );
end decodificador;

architecture dataflow of decodificador is
begin
    with to_integer(unsigned(code_in)) select
        code_out <= "1110" when 0,
                    "1101" when 1,
                    "1011" when 2,
                    "0111" when others;
end dataflow;

